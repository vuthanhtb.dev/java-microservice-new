package com.dev.productservice.service;

import com.dev.productservice.dto.request.ProductRequest;
import com.dev.productservice.dto.response.ProductResponse;
import com.dev.productservice.mapper.ProductMapper;
import com.dev.productservice.model.Product;
import com.dev.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductResponse createProduct(ProductRequest productRequest) {
        Product product = this.productMapper.toProduct(productRequest);
        Product savedProduct = this.productRepository.save(product);
        return this.productMapper.toProductResponse(savedProduct);
    }

    public List<ProductResponse> getAllProducts() {
        List<Product> products = this.productRepository.findAll();
        return this.productMapper.toProductResponseList(products);
    }

}
