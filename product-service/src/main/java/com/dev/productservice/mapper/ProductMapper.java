package com.dev.productservice.mapper;

import com.dev.productservice.dto.request.ProductRequest;
import com.dev.productservice.dto.response.ProductResponse;
import com.dev.productservice.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ProductMapper {
    public abstract ProductResponse toProductResponse(Product product);

    @Mapping(target = "id", ignore = true)
    public abstract Product toProduct(ProductRequest productRequest);

    public List<ProductResponse> toProductResponseList(List<Product> products) {
        if (null == products) {
            return null;
        }
        return products.stream().map(this::toProductResponse).toList();
    }
}
