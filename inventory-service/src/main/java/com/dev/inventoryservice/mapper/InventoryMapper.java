package com.dev.inventoryservice.mapper;

import com.dev.inventoryservice.dto.InventoryResponse;
import com.dev.inventoryservice.model.Inventory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface InventoryMapper {

    @Mapping(target = "inStock", expression = "java(mapQuantity(inventory))")
    InventoryResponse toInventoryResponse(Inventory inventory);

    default boolean mapQuantity(Inventory inventory) {
        return inventory.getQuantity() > 0;
    }

    default List<InventoryResponse> toInventoryResponseList(List<Inventory> inventories) {
        if (null == inventories) {
            return null;
        }
        return inventories.stream().map(this::toInventoryResponse).toList();
    }
}
