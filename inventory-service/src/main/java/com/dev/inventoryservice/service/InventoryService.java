package com.dev.inventoryservice.service;

import com.dev.inventoryservice.dto.InventoryResponse;
import com.dev.inventoryservice.mapper.InventoryMapper;
import com.dev.inventoryservice.model.Inventory;
import com.dev.inventoryservice.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InventoryService {
    private final InventoryRepository inventoryRepository;
    private final InventoryMapper inventoryMapper;

    @Transactional(readOnly = true)
    @SneakyThrows
    public List<InventoryResponse> isInStock(List<String> skuCode) {
        List<Inventory> inventories = this.inventoryRepository.findBySkuCodeIn(skuCode);
        return this.inventoryMapper.toInventoryResponseList(inventories);
    }
}
