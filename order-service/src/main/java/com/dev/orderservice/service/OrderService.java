package com.dev.orderservice.service;

import brave.Span;
import brave.Tracer;
import com.dev.orderservice.dto.InventoryResponse;
import com.dev.orderservice.dto.OrderRequest;
import com.dev.orderservice.event.OrderPlacedEvent;
import com.dev.orderservice.mapper.OrderMapper;
import com.dev.orderservice.model.Order;
import com.dev.orderservice.model.OrderLineItems;
import com.dev.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.dev.orderservice.constant.ClientConstant.INVENTORY_CLIENT;
import static com.dev.orderservice.constant.TopicConstant.NOTIFICATION_TOPIC;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final WebClient.Builder webClientBuilder;
    private final Tracer tracer;
    private final KafkaTemplate<String, OrderPlacedEvent> kafkaTemplate;

    public String placeOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLineItems> orderLineItems = this.orderMapper.toOrderLineItemsList(orderRequest.getOrderLineItemsDtoList());

        order.setOrderLineItemsList(orderLineItems);

        List<String> skuCodes = order.getOrderLineItemsList()
                .stream()
                .map(OrderLineItems::getSkuCode)
                .toList();

        Span inventoryServiceLookup = this.tracer.nextSpan().name("InventoryServiceLookup");

        try (Tracer.SpanInScope isLookup = this.tracer.withSpanInScope(inventoryServiceLookup.start())) {

            inventoryServiceLookup.tag("call", "inventory-service");

            // Call Inventory Service, and place order if product is in stock
            InventoryResponse[] inventoryResponses = this.webClientBuilder
                    .build()
                    .get()
                    .uri(INVENTORY_CLIENT, uriBuilder -> uriBuilder.queryParam("skuCode", skuCodes).build())
                    .retrieve()
                    .bodyToMono(InventoryResponse[].class)
                    .block();

            long inventoryResponseCount = inventoryResponses == null ? 0 : Arrays.stream(inventoryResponses).count();

            boolean allProductsInStock = inventoryResponseCount > 0 && Arrays.stream(inventoryResponses).allMatch(InventoryResponse::isInStock);

            if (allProductsInStock) {
                this.orderRepository.save(order);
                this.kafkaTemplate.send(NOTIFICATION_TOPIC, new OrderPlacedEvent(order.getOrderNumber()));
                return "Order Placed Successfully";
            }

            throw new IllegalArgumentException("Product is not in stock, please try again later");
        } finally {
            inventoryServiceLookup.flush();
        }
    }
}
