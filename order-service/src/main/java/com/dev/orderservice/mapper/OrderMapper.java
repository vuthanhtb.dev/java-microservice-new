package com.dev.orderservice.mapper;

import com.dev.orderservice.dto.OrderLineItemsDto;
import com.dev.orderservice.model.OrderLineItems;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class OrderMapper {
    @Mapping(target = "id", ignore = true)
    public abstract OrderLineItems toOrderLineItems(OrderLineItemsDto orderLineItemsDto);

    public List<OrderLineItems> toOrderLineItemsList(List<OrderLineItemsDto> orderLineItemsDtos) {
        if (null == orderLineItemsDtos) {
            return null;
        }
        return orderLineItemsDtos.stream().map(this::toOrderLineItems).toList();
    }
}
